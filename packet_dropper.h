#ifndef PACKET_DROPPER_H
#define PACKET_DROPPER_H

#include "bpf_logic.h"
#include "hook.h"

int initialize_predefined_bpf_prog(void);
void deinitialize_predefined_bpf_prog(void);

extern struct ftrace_hook packet_dropper_hooks[1];

#endif
