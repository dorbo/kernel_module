#include <linux/filter.h>
#include <linux/skbuff.h>

#include "packet_dropper.h"

#define DROP_PACKET 0
#define SK_CLONE_ERR 1 // unsigned, !=0(DROP_PACKET) - pass packet

static struct bpf_prog *predefined_prog;

// Initialize predefined filter for running over all entered packets
int initialize_predefined_bpf_prog(void)
{
	return get_predefined_bprog(&predefined_prog);
}

// Deinitialize predefined filter for running over all entered packets
void deinitialize_predefined_bpf_prog(void)
{
	bpf_prog_destroy(predefined_prog);
}

// Run predefined filter over entered packet
static u32 run_filter(struct sk_buff *skb, struct bpf_prog *prog)
{
	u32 act;
	struct sk_buff *skb2 = skb_clone(skb, GFP_ATOMIC);
	if (!skb2) {
		pr_err("error cloning skb");
		return SK_CLONE_ERR;
	}
	skb2->data = skb_mac_header(skb2);
	
	preempt_disable();
	rcu_read_lock();
	act = bpf_prog_run_clear_cb(rcu_dereference(prog), skb2);
	rcu_read_unlock();
	preempt_enable();

	kfree_skb(skb2);
	return act;
}


/*
	netif_receive_skb is called for each entered packet
	hooked for checking if the packet passes our filter, if not - drop
*/
static asmlinkage int (*real_netif_receive_skb_internal) (struct sk_buff *skb);
static asmlinkage int hooked_netif_receive_skb_internal(struct sk_buff *skb)
{
	u32 act;
	act = run_filter(skb, predefined_prog);
	if (act == DROP_PACKET) {
		pr_info("packet dropped!");
		kfree_skb(skb);
		return NET_RX_DROP;
	}

	return real_netif_receive_skb_internal(skb);
}

// set hooks struct array
struct ftrace_hook packet_dropper_hooks[] = {
	HOOK("netif_receive_skb_internal", hooked_netif_receive_skb_internal, &real_netif_receive_skb_internal),
};
