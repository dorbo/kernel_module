#ifndef BPF_LOGIC_H
#define BPF_LOGIC_H

#include <linux/filter.h>

int get_predefined_bprog(struct bpf_prog **pprog);

#endif
