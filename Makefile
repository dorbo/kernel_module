obj-m += packet_hider_module.o
packet_hider_module-objs := bpf_logic.o hook.o module_hider.o packet_dropper.o packet_hider.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean
