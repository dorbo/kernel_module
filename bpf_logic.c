#include "bpf_logic.h"

/*
	allocate_fprog_and_filter - allocate memory for fprog struct and it's filter
	@pfprog: pointer to pointer to the new allocated sock_fprog_kern
	@filter_len: the length of the new allocated fprog's filter

	This function allocates memory for fprog struct and it's filter and puts the new fprog pointer in *ppredefined_fprog.
*/
static int allocate_fprog_and_filter(struct sock_fprog_kern **pfprog, int filter_len)
{
	struct sock_fprog_kern *fprog;
	fprog = kmalloc(sizeof(struct sock_fprog_kern), GFP_KERNEL);
	if (!fprog)
		return -ENOMEM;
	
	fprog->filter = kmalloc(filter_len * sizeof(struct sock_filter), GFP_KERNEL);
	if (!fprog->filter) {
		kfree(fprog);
		return -ENOMEM;
	}

	*pfprog = fprog;
	return 0;
}

/*
	get_predefined_fprog - get the predefined fprog
	@ppredefined_fprog: pointer to pointer to the predefined sock_fprog_kern

	This function copies the hardcoded predefined_fprog to allocated memory and puts the new fprog pointer in *ppredefined_fprog.
*/
static int get_predefined_fprog(struct sock_fprog_kern **ppredefined_fprog)
{
	int err, fsize;
	struct sock_fprog_kern * predefined_fprog;
	/*
		Result of $ tcpdump -dd "not tcp port 8080"
	*/
	struct sock_filter filter[] = {
		{ 0x28, 0, 0, 0x0000000c },
		{ 0x15, 0, 6, 0x000086dd },
		{ 0x30, 0, 0, 0x00000014 },
		{ 0x15, 0, 15, 0x00000006 },
		{ 0x28, 0, 0, 0x00000036 },
		{ 0x15, 12, 0, 0x00001f90 },
		{ 0x28, 0, 0, 0x00000038 },
		{ 0x15, 10, 11, 0x00001f90 },
		{ 0x15, 0, 10, 0x00000800 },
		{ 0x30, 0, 0, 0x00000017 },
		{ 0x15, 0, 8, 0x00000006 },
		{ 0x28, 0, 0, 0x00000014 },
		{ 0x45, 6, 0, 0x00001fff },
		{ 0xb1, 0, 0, 0x0000000e },
		{ 0x48, 0, 0, 0x0000000e },
		{ 0x15, 2, 0, 0x00001f90 },
		{ 0x48, 0, 0, 0x00000010 },
		{ 0x15, 0, 1, 0x00001f90 },
		{ 0x6, 0, 0, 0x00000000 },
		{ 0x6, 0, 0, 0x00040000 },
	};
	fsize = ARRAY_SIZE(filter);
	
	err = allocate_fprog_and_filter(&predefined_fprog, fsize);
	if (err < 0)
		return err;
	
	predefined_fprog->len = fsize;
	memcpy(predefined_fprog->filter, filter, fsize * sizeof(struct sock_filter));
	
	*ppredefined_fprog = predefined_fprog;
	return 0;
}


/*
	get_predefined_bprog - get the predefined bprog, created using get_predefined_fprog
	@pprog: pointer to pointer to the predefined bpf_prog

	This function creates bprog from the predefined_fprog
*/
int get_predefined_bprog(struct bpf_prog **pprog)
{
	int err;
	struct sock_fprog_kern *predefined_fprog;
	
	err = get_predefined_fprog(&predefined_fprog);
	if (err < 0)
		return err;
	
	return bpf_prog_create(pprog, predefined_fprog);
}
